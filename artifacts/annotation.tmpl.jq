{
    ($input_file): {
        "org.opencontainers.image.title": $output_file,
        "org.opencontainers.image.created": $date,
        "org.opencontainers.image.description": $file_description
    },
    "$config": {
        "org.opencontainers.image.created": $date
    },
    "$manifest": {
        "org.opencontainers.image.title": "CORScanner",
        "org.opencontainers.image.description": "A python tool designed to discover CORS misconfigurations vulnerabilities of websites",
        "org.opencontainers.image.vendor": "rdnxk",
        "org.opencontainers.image.authors": "rdnxk, San 'rdn' Mônico <san@monico.com.br>",
        "org.opencontainers.image.url": "https://hub.docker.com/r/redemonbr/corscanner",
        "org.opencontainers.image.source": "https://gitlab.com/rdnxk/CORScanner",
        "org.opencontainers.image.documentation": "https://gitlab.com/rdnxk/CORScanner/-/blob/master/container/docker.md",
        "org.opencontainers.image.created": $date
    }
}
