### MAKE CONFIG
TEMP_DIR :::= .tmp

## UTILS
comma :::= ,
TAG_DEFAULT :::= bookworm
CONFIG_TYPE :::= application/vnd.rdnxk.manifest.config.v1+json
PEM_TYPE :::= application/x-pem-file
MARKDOWN_TYPE :::= text/markdown
SYFT_JSON_TYPE :::= application/vnd.syft+json
PUBLICKEY_DESCRIPTION :::= PEM-formatted public key for validating images and artifacts built by rdnxk and ReDemoNBR
LICENSE_DESCRIPTION :::= License for this image
README_DESCRIPTION :::= Usage of this image
SBOM_DESCRIPTION :::= Software bill of materials of this image, in Syft-JSON format
SUBMODULE_PATH :::= ./CORScanner
