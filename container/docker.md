## Supported tags and `Containerfile` links
-   [`1.0.1-bookworm`, `1.0-bookworm`, `1-bookworm`, `1.0.1`, `1.0`, `1`, `bookworm`, `latest`](https://gitlab.com/rdnxk/CORScanner/-/blob/master/container/debian/Containerfile)
-   [`1.0.1-alpine`, `1.0-alpine`, `1-alpine`, `alpine`](https://gitlab.com/rdnxk/CORScanner/-/blob/master/container/alpine/Containerfile)

## Image Variants
### `<version>` | `<version>-bookworm`
This is the defacto image. If you are unsure about what your needs are, you probably want to use this one.
These images are based on Bullseye release of Debian.

### `<version>-alpine`
This image is based on the popular [Alpine Linux](https://alpinelinux.org/) which is much smaller than most distribution base images (~5MB), and thus leads to much slimmer images in general.

## Source of these images
-   Repository: <https://gitlab.com/rdnxk/CORScanner>
-   Issues: <https://gitlab.com/rdnxk/CORScanner/-/issues>

These images are automatically updated on a weekly basis.
Attestations, SBOM and Signatures are attached to the image digests:
-   **Attestation**: Attestation predicate is attached with [SPDX](https://spdx.dev) in JSON (`application/spdx+json`) format. SPDX is an open standard widely used for software package data exchange
-   **SBOM**: SBOM is attached with [Syft](https://github.com/anchore/syft) in JSON (`application/vnd.syft+json`) format. Syft SBOMs are good for using in conjunction with [Grype](https://github.com/anchore/grype) for vulnerability scanning

### OCI Artifacts
Other OCI artifacts are pushed to this repository. They include the License, Public Key and README with the following tags
-   **license**: Refer to the latest license
-   **license_&lt;DATE&gt;**: Refer to the license that was pushed at specified `DATE` datetime. Useful if the latest license was updated
-   **publickey**: Refer to the latest public key
-   **publickey_&lt;DATE&gt;**: Refer to the public key that was pushed at specified `DATE` datetime. Useful when the keys used to sign the old image and artificats got rotated
-   **readme**: Refer to the lastest README
-   **readme_&lt;DATE&gt;**: Refer to the README that was pushed at the specified `DATE` datetime. Useful if the latest README was updated.

The public keys can be used to validate the authenticity of the images and artifacts. They can be used as below
```console
Display signatures and artifacts related to image
$ cosign triangulate docker.io/redemonbr/corscanner:latest
$ cosign tree docker.io/redemonbr/corscanner:latest

Pulls/Downloads the public key (it will be saved as cosign.pub)
$ oras pull docker.io/redemonbr/corscanner:publickey
Verify image (via manifest/tag or digest), SBOM and attestation
$ cosign verify --key cosign.pub docker.io/redemonbr/corscanner:latest
$ cosign verify --key cosign.pub docker.io/redemonbr/corscanner@sha256:...
$ cosign verify --attachment sbom --key cosign.pub docker.io/redemonbr/corscanner@sha256:...
$ cosign verify-attestation --type spdxjson --key cosign.pub docker.io/redemonbr/corscanner@sha256:...
```
